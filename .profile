# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
# umask 022

# first, let's tweak our PATH
if [ -z "$PROFILE_SET" ]; then

  umask 022

  export PROFILE_SET=y
  export LESS=-MM

  LD_LIBRARY_PATH="/usr/local/lib"

  if [ ! -f /etc/cygport.conf ]; then
    MANPATH=${HOME}/opt/man
    which manpath > /dev/null 2>&1
    [ $? = 0 ] && MANPATH=${MANPATH}:$(manpath)
  fi
  #if [ -x /usr/bin/manpath ]; then
  #  MANPATH="${HOME}/opt/man:$(manpath)"
  #else
  #  MANPATH="${HOME}/opt/man"
  #fi

  export EDITOR=emacs
  export VISUAL=emacs
  export PAGER=/usr/bin/less
  export SYSTEMD_PAGER=""
  export PYTHONSTARTUP="${HOME}/.pythonrc.py"
  export PYTHONUSERBASE="${HOME}/opt/python"
  export WORKON_HOME="${HOME}/opt/python/virtualenvs"
  export PIP_VIRTUALENV_BASE=$WORKON_HOME
  export VAGRANT_DEFAULT_PROVIDER=libvirt

  #TERM="vt102"
  #export TERM

  #export GYP_GENERATORS='ninja'
  #export GIT_EDITOR='emacs'
  #export GIT_PAGER=''

  #CLUTTER_PAINT=disable-clipped-redraws:disable-culling
  #CLUTTER_VBLANK=True
  #export CLUTTER_PAINT CLUTTER_VBLANK

  PATH="${HOME}/opt/bin:$PATH"
  if [ -f /etc/lsb-release ]; then
    . /etc/lsb-release
  elif [ -f /etc/os-release ]; then
    . /etc/os-release
    DISTRIB_CODENAME="${ID}${VERSION_ID}"
  elif [ -f /etc/cygport.conf ]; then
    if [ "$HOSTNAME" = "jaypeak" ]; then
      PATH=${PATH}:/usr/local/bin:/usr/bin
    fi
    ## TortoiseSVN (on windows)
    if [ -d "/cygdrive/c/Program Files/TortoiseSVN/bin" ]; then
      # put tsvn client ahead of cygwin, since cygwin's doesn't
      # really handle ACL stuff correctly (basically it gets the
      # executable permission bits wrong on a lot of things)
      PATH="/cygdrive/c/Program Files/TortoiseSVN/bin:$PATH"
    fi
    USERS=/cygdrive/c/Users
    PLATFORMTOOLS=AppData/Local/Android/sdk/platform-tools
    for u in phil philipf; do
      if [ -d "${USERS}/${u}/${PLATFORMTOOLS}" ]; then
        PATH="$PATH:${USERS}/${u}/${PLATFORMTOOLS}"
      fi
    done
    if [ -d "/cygdrive/c/Program Files/Oracle/VirtualBox" ]; then
      PATH="$PATH:/cygdrive/c/Program Files/Oracle/VirtualBox"
    fi
    DISTRIB_CODENAME="CYGWIN_NT-10.0"
    [ -z "$DISPLAY" ] && export DISPLAY=:0.0
    ulimit -c 0
  fi

  ## firma or arm
  if [ -d ${HOME}/opt/CentOS-6.5/x86_64/bin ]; then
    PATH="${HOME}/opt/CentOS-6.5/x86_64/bin:${PATH}:/sbin"
  fi
  if [ -d ${HOME}/opt/CentOS-6.5/x86_64/emacs/25.3/bin ]; then
    PATH="${HOME}/opt/CentOS-6.5/x86_64/emacs/25.3/bin:$PATH"
  fi
  ## dev08
  if [ -d ${HOME}/opt/CentOS-8.1/x86_64/bin ]; then
    PATH="${HOME}/opt/CentOS-8.1/x86_64/bin:${PATH}:/sbin"
  fi
  if [ -d ${HOME}/opt/CentOS-8.1/x86_64/emacs/25.3/bin ]; then
    PATH="${HOME}/opt/CentOS-8.1/x86_64/emacs/25.3/bin:$PATH"
  fi
  if [ -d ${HOME}/ebw/gdb.arm/bin ]; then
    PATH="${PATH}:${HOME}/ebw/gdb.arm/bin"
  fi

  if [ ! -z "$DISTRIB_CODENAME" ]; then
    arch=${DISTRIB_CODENAME}/$(uname -m)
    PATH="${HOME}/opt/${arch}/emacs/25.3/bin:$PATH"
    PATH="${HOME}/opt/${arch}/bin:$PATH"
    MANPATH="${MANPATH}:${HOME}/opt/${arch}/25.3/emacs/share/man"
    #MANPATH="${MANPATH}:${HOME}/opt/${arch}/man"
    MANPATH="${MANPATH}:${HOME}/opt/${arch}/share/man"
  fi

  # NB: must follow previous if clause
  if [ -d ${HOME}/ebw/svn-1.6.11/bin ]; then
    # we need to use an older version of svn when working with eb
    # repos on brome
    LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${HOME}/ebw/svn-1.6.11/lib"
    PATH="${HOME}/ebw/svn-1.6.11/bin:${PATH}"
  fi

  PATH=${HOME}/opt/nodejs/$(uname -m)/bin:$PATH
  PATH="${HOME}/opt/python/bin:$PATH"
  PATH="${HOME}/bin:$PATH"
  GLASSFISH_HOME="${HOME}/java/glassfish4"
  [ -d ${GLASSFISH_HOME}/bin ] && PATH="${PATH}:${GLASSFISH_HOME}/bin"
  [ -d ${GLASSFISH_HOME}/glassfish/bin ] && \
    PATH="${PATH}:${GLASSFISH_HOME}/glassfish/bin"
  MAVEN_HOME="${HOME}/java/apache-maven-3.3.3"
  [ -d ${MAVEN_HOME} ] && PATH="${PATH}:${MAVEN_HOME}/bin"
  GRADLE_HOME="${HOME}/java/gradle"
  [ -d ${GRADLE_HOME} ] && PATH="${PATH}:${GRADLE_HOME}/bin"
  NETBEANS_HOME="${HOME}/java/netbeans"
  [ -d ${NETBEANS_HOME} ] && PATH="${PATH}:${NETBEANS_HOME}/bin"
  JAVA_HOME="${HOME}/java/jdk8"
  if [ -d ${JAVA_HOME} ]; then
    PATH="${JAVA_HOME}/bin:${PATH}"
    export JAVA_HOME
  fi
  GROOVY_HOME="${HOME}/java/groovy"
  if [ -d ${GROOVY_HOME} ]; then
    PATH="${GROOVY_HOME}/bin:${PATH}"
    export GROOVY_HOME
  fi
  WILDFLY_HOME="${HOME}/java/wildfly"
  if [ -d ${WILDFLY_HOME} ]; then
    PATH="${WILDFLY_HOME}/bin:${PATH}"
    export WILDFLY_HOME
  fi
  GOROOT="${HOME}/opt/go"
  if [ -d ${GOROOT} ]; then
    PATH="${GOROOT}/bin:${PATH}"
    export GOPATH=${GOROOT}
    export GOROOT
  fi

  PROTOC_HOME="${HOME}/opt/protoc"
  if [ -d ${PROTOC_HOME} ]; then
    PATH="${PATH}:${PROTOC_HOME}/bin"
    export PROTOC_HOME
  fi

  #CONNECTORJ="${HOME}/java/mysql-connector-java"

  #if [ `hostname` = "elephant" ]; then
  #  PATH="$PATH:~/src/hacking/ct-ng/bin"
  #fi
  #PATH="$PATH:${HOME}/opt/depot_tools"

  if [ "$(hostname)" = "firma.dev.ecobee.com" ]; then
    PATH=$(echo $PATH | sed 's,:/usr/local/bin,,g')
    PATH="$PATH:/usr/local/bin"
    PATH="/opt/gcc-8.2/bin:$PATH"
    LD_LIBRARY_PATH="/opt/gcc-8.2/lib:/opt/gcc-8.2/lib64:$LD_LIBRARY_PATH"
  fi

  if [ -d "${HOME}/.linuxbrew" ]; then
    export HOMEBREW_PREFIX="/home/philipf/.linuxbrew"
    export HOMEBREW_CELLAR="/home/philipf/.linuxbrew/Cellar"
    export HOMEBREW_REPOSITORY="/home/philipf/.linuxbrew/Homebrew"
    export MANPATH="/home/philipf/.linuxbrew/share/man${MANPATH+:$MANPATH}:"
    export INFOPATH="/home/philipf/.linuxbrew/share/info${INFOPATH+:$INFOPATH}"
    PATH="/home/philipf/.linuxbrew/sbin${PATH+:$PATH}"
    PATH="/home/philipf/.linuxbrew/bin${PATH+:$PATH}"
  fi

  export LD_LIBRARY_PATH
  export MANPATH
  export PATH

  #shopt -s huponexit

fi

# if running bash
if [ -n "$BASH_VERSION" ]; then
  # include .bashrc if it exists
  if [ -f "$HOME/.bashrc" ]; then
	  . "$HOME/.bashrc"
  fi
  # only after PATH has been set
  if [ -f ${HOME}/opt/python/bin/virtualenvwrapper.sh ]; then
    . ${HOME}/opt/python/bin/virtualenvwrapper.sh
    if [ ! -z "$VIRTUAL_ENV" ]; then
      PS1="($(basename \"$VIRTUAL_ENV\"))$PS1"
    fi
  fi
fi
