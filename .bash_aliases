psg () {

  pids=
  if [ ! -z "$*" ]; then
    pids=$(pgrep -f $*)
    [ ! -z "$pids" ] && ps -f ${pids}
  else
    ps -f ${pids}
  fi

}

alias mountdl='sshfs -o idmap=user jaypeak://home/phil/Downloads /home/phil/Downloads'
alias umountdl='fusermount -u /home/phil/Downloads'

alias vpnfw='sudo iptables -A OUTPUT -o eth0 -m owner --uid-owner 1000 -j DROP'
alias vpnfwclr='sudo iptables -F'
