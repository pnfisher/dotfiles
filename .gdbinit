#set auto-load safe-path /
set print pretty on
set confirm off
set pagination off
set non-stop off
set verbose on
set history save on
set history expansion on
set history size 1024
#set follow-fork-mode child
#set detach-on-fork off
#set follow-exec-mode new

add-auto-load-safe-path /home/philipf/ebw/homeservice/iotcore/.gdbinit
