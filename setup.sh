#!/bin/bash

scripts=$(find `pwd`/ -type f -name ".*")

for s in ${scripts}; do

  (cd ~/; ln -sf $s $(basename $s))

done

for f in ~/.bash_profile ~/.bash_login; do
  if [ -f $f ]; then
    echo "WARNING: renaming $f to $f.bak" 1>&2
    rm -f $f.bak
    mv $f $f.bak
  fi
done

dotemacs=${HOME}/src/elisp/.emacs
if [ ! -f ${dotemacs} ]; then
  echo "error, ${dotemacs} missing" 1>&2
  exit 1
fi
ln -sf ${dotemacs} ${HOME}/.emacs
